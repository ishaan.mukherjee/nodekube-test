const express = require('express')
var axios = require('axios');
const app = express()
const port = 3000

app.get('/', (req, res) => {

    axios.get('http://127.0.0.1:3000')
    .then(response => {
        res.send(response.data);
    })
    .catch(err => console.log(err))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})